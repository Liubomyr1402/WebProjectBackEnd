﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using WebApplicationDAL.Entities;
using System.Linq;

namespace WebApplicationDAL.EF
{
    public class BookPurchaseDbContext : DbContext
    {
        public DbSet<Book> Books { get; set; }

        public DbSet<Purchase> Purchases { get; set; }

        public BookPurchaseDbContext(DbContextOptions dbContextOptions)
            : base(dbContextOptions)
        {

        }
    }
}
