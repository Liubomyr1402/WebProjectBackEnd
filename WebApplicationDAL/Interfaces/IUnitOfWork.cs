﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplicationDAL.Entities;

namespace WebApplicationDAL.Interfaces
{
    public interface IDBUnitOfWork
    {
        IRepository<Book> Books { get; }

        IRepository<Purchase> Purchase { get; }

        void Save();
    }
}
