﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WebApplicationDAL.EF;
using WebApplicationDAL.Entities;
using WebApplicationDAL.Repositories;
using WebApplicationDAL.Interfaces;

namespace WebApplication.Controllers
{
    [Route("api/Purchase")]
    public class PurchaseController : Controller
    {
        BookPurchaseDbContext dbContext;
        IDBUnitOfWork dbUnitOfWork;

        public PurchaseController(BookPurchaseDbContext dbContext)
        {
            this.dbContext = dbContext;

            dbUnitOfWork = new DBUnitOfWork(this.dbContext);

        }

        [HttpGet]
        public IEnumerable<Purchase> Get()
        {
            return dbUnitOfWork.Purchase.GetAll();
        }

        [HttpGet("{id}")]
        public Purchase Get(int id)
        {
            Purchase newPurchase = dbUnitOfWork.Purchase.Get(id);

            return newPurchase;
        }

        [HttpPost]
        public void Post([FromBody]Purchase newPurchase)
        {
            if (newPurchase.Person == null)
            {
                Response.StatusCode = 204;
            }
            else
            {
                dbUnitOfWork.Purchase.Create(newPurchase);
                dbUnitOfWork.Save();
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Purchase purchaseToUpdate)
        {
            if (purchaseToUpdate.Person == null || purchaseToUpdate.PurchaseId != id)
            {
                Response.StatusCode = 204;
            }
            else
            {
                dbUnitOfWork.Purchase.Update(purchaseToUpdate);
                dbUnitOfWork.Save();
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            if (dbUnitOfWork.Purchase.Get(id) == null)
            {
                Response.StatusCode = 204;
            }
            else
            {
                dbUnitOfWork.Purchase.Delete(id);
                dbUnitOfWork.Save();
            }
        }
    }
}